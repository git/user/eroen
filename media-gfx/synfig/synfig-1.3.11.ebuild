# By eroen <eroen-overlay@occam.eroen.eu>, 2018
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

EAPI=6

inherit cmake-utils

DESCRIPTION="Vector based 2D animation package"
HOMEPAGE="https://www.synfig.org"
SRC_URI="https://github.com/synfig/synfig/archive/v$PV.tar.gz -> synfig-$PV.tar.gz"
S=${WORKDIR}/synfig-$PV/synfig-core

LICENSE="GPL-2+"

SLOT="0"
KEYWORDS="~amd64"
IUSE=""
#IUSE="imagemagick"

RDEPEND="
	dev-libs/boost
	sys-libs/zlib
	dev-libs/libsigc++:2
	dev-cpp/glibmm:2
	x11-libs/cairo
	dev-cpp/libxmlpp:2.6
	media-libs/mlt
	sci-libs/fftw:3.0
	x11-libs/pango
	media-gfx/imagemagick[cxx]
	"
DEPEND="${RDEPEND}
	dev-cpp/ETL
	virtual/pkgconfig
	"

PATCHES=(
	"$FILESDIR"/synfig-1.3.10-install-libraries-to-lib64.patch
	"$FILESDIR"/fix-ffmpeg-with-cmake-build.patch
	)

src_configure() {
	local mycmakeargs=(
		-DWITH_MAGICPP=ON
		)
	cmake-utils_src_configure
}

## Always-enabled modules:
# lyr_freetype
#   pango/pangocairo.h
#   fontconfig.h
#   ft2build.h
# lyr_std
# mod_bmp
# mod_dv
# mod_example
# mod_ffmpeg
# mod_filter
# mod_geometry
# mod_gif
# mod_gradient
# mod_imagemagick
# mod_jpeg
#   jpeglib.h
# mod_noise
# mod_particle
# mod_png
#   cairo.h
#   png.h
# mod_ppm
# mod_svg
# mod_yuv420p

## Optional modules:
# mod_magickpp: WITH_MAGICPP WITHOUT_MAGICPP
#   Magick++.h

## Disabled modules:
# mod_libavcodec
#   avformat.h
#   swscale.h
# mod_mng
#   libmng.h
# mod_openexr
#   ImfArray.h
#   ImfRgbaFile.h
# mptr_mplayer
