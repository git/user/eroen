# By eroen <eroen-overlay@occam.eroen.eu>, 2018
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

EAPI=6

inherit cmake-utils

DESCRIPTION="Vector animation studio"
HOMEPAGE="https://www.synfig.org"
SRC_URI="https://github.com/synfig/synfig/archive/v$PV.tar.gz -> synfig-$PV.tar.gz"
S=${WORKDIR}/synfig-$PV/synfig-studio

LICENSE="GPL-2+ CC-BY-3.0"

SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
	media-gfx/synfig
	dev-libs/libsigc++:2
	dev-cpp/gtkmm:3.0
	dev-cpp/libxmlpp:2.6
	sys-devel/gettext
	"
DEPEND="${RDEPEND}
	dev-cpp/ETL
	virtual/pkgconfig
	"

PATCHES=(
	"$FILESDIR"/synfigstudio-1.3.10-install-libraries-to-lib64.patch
	)

src_configure() {
	CPPFLAGS+=' -DIMAGE_DIR=\"\\\"/usr/share/pixmaps\\\"\"'
	cmake-utils_src_configure
}

src_compile() {
	cmake-utils_src_compile all
	cmake-utils_src_compile build_images
}
